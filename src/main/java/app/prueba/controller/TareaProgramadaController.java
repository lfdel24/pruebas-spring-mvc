package app.prueba.controller;

import java.util.Date;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/tp")
public class TareaProgramadaController {

    @RequestMapping(value = {"", "/"})
    public String index(Model model) {
        return "tp";
    }

    @RequestMapping(value = "/saludar")
    @ResponseBody
    public String saludar(Model model) {
        Date date = new Date("YYYY-MM-DD");
        System.out.println(date.toString());
        return date.toString();
    }

}
