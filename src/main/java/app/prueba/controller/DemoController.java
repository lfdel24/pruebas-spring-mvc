package app.prueba.controller;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/demo")
public class DemoController {

    public String data = "0";
    private boolean ejecutar = true;

    @RequestMapping(value = {"", "/"})
    public String index(Model model) {
        return "demo";
    }

    @RequestMapping(value = "/a")
    @ResponseBody
    public String demo(Model model) {

        if (ejecutar) {
            ejecutar = false;
            System.out.println("Creating Runnable...");
            Runnable runnable = () -> {
                System.out.println("Inside : " + Thread.currentThread().getName());
                for (int i = 0; i < 100; i++) {
                    data = String.valueOf(i);
                    System.out.println(data);
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(DemoController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            };

            System.out.println("Creating Thread...");
            Thread thread = new Thread(runnable);

            System.out.println("Starting Thread...");
            thread.start();
        }

        return data;
    }

    
    
}
