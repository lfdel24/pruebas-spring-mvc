package app.prueba;

import java.util.Date;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class Application {

    private final int UN_SEGUNDO = 1000;
    //second, minute, hour, day of month, month, day(s) of week
    //info: https://www.baeldung.com/cron-expressions
    private final String ONCE_EN_PUNTO = "0 0 11 * * *";

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Scheduled(fixedDelay = UN_SEGUNDO)
    public void cadaSegundo() {
        Date date = new Date();
        System.out.println("Tarea ejecutada cada segundo: " + date.toString());
    }

    
    @Scheduled(cron = ONCE_EN_PUNTO, zone = "America/Bogota")
    public void cadaHoraDia() {
        Date date = new Date();
        System.out.println("Tarea ejecutada a las 11 en punto: " + date.toString());
    }
}
